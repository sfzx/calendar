import React from 'react'
import { Col } from 'react-bootstrap'

export const Calendar = ({ time }) => {
  return (

    <Col className='calendar-border' style={{ height: 180 }}>
      <p className='time' style={{ fontSize: 16 }}>
        {time}:00
      </p>

      {time !== 5 &&
        <p className='time vertical-center' style={{ fontSize: 12 }}>
          {time}:30
        </p>}

    </Col>

  )
}
