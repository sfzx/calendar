import React, { useContext, useState } from 'react'
import { Nav, Navbar, Button } from 'react-bootstrap'
import { FaRegCalendarPlus } from 'react-icons/fa'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { AuthContext } from '../context/AuthContext'
import { CreateEventModal } from '../modal/CreateEventModal'
import { downloadJSON } from '../helpers/download'

export const MyNavbar = () => {
  const [modalShow, setModalShow] = useState(false)
  const history = useHistory()
  const auth = useContext(AuthContext)

  const logoutHandler = (event) => {
    event.preventDefault()
    auth.logout()
    history.push('/')
  }

  const downloadHandler = async (event) => {
    event.preventDefault()

    downloadJSON(events)
  }

  const events = useSelector(state => state.event)

  return (
    <Navbar bg='primary' variant='dark'>
      <Navbar.Brand href='/'>Calendar</Navbar.Brand>
      <Nav className='justify-content-end'>
        {auth.isAuthenticated
          ? <>
            <Nav.Item>
              <Button onClick={() => setModalShow(true)}>
                <FaRegCalendarPlus />
              </Button>
            </Nav.Item>
            <CreateEventModal
              show={modalShow}
              onHide={() => setModalShow(false)}
            />
            <Nav.Item>
              <Button onClick={downloadHandler}>
                Download events
              </Button>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href='/' onClick={logoutHandler}>
                LogOut
              </Nav.Link>
            </Nav.Item>
            </>
          : <>
            <Nav.Item>
              <Nav.Link href='/login'>Login</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href='/register'>Register</Nav.Link>
            </Nav.Item>
            </>}
      </Nav>
    </Navbar>
  )
}
