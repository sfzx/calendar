import React, { useState } from 'react'
import { Modal, Button } from 'react-bootstrap'
import { useHttp } from '../hooks/http.hook'
import { deleteEvent } from '../data/redux/events/reducers'
import { useDispatch } from 'react-redux'

export const Event = ({ data }) => {
  const dispatch = useDispatch()
  const { request } = useHttp()
  const [show, setShow] = useState(false)

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)
  const handleDelete = async () => {
    await request('/api/events/delete-event', 'DELETE', { eventId: data.id, owner: data.owner })
    dispatch(deleteEvent({ id: data.id }))
    setShow(false)
    window.location.reload(false)
  }

  return (
    <>
      <div className='event-container' style={{ height: data.duration * 3, cursor: 'pointer', marginTop: data.start * 3 }} onClick={handleShow}>
        <div className='event-title'>{data.title}</div>
      </div>
      <Modal
        show={show}
        onHide={handleClose}
        size='lg'
        aria-labelledby='contained-modal-title-vcenter'
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id='contained-modal-title-vcenter'>
            Event
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h1>
            {data.title}
          </h1>
          <Button onClick={handleDelete}>
            Delete
          </Button>
        </Modal.Body>
      </Modal>
    </>
  )
}
