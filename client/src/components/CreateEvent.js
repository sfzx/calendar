import React, { useContext, useState, useEffect } from 'react'
import { Form, Button } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import RandExp from 'randexp'
import { AuthContext } from '../context/AuthContext'
import { useHttp } from '../hooks/http.hook'
import { useMessage } from '../hooks/message.hook'
import { addEvent } from '../data/redux/events/reducers'

export const CreateEvent = ({ onHide }) => {
  const dispatch = useDispatch()
  const auth = useContext(AuthContext)
  const message = useMessage()
  const { loading, request, error, clearError } = useHttp()
  const [form, setForm] = useState({
    title: '', start: '', duration: ''
  })

  useEffect(() => {
    message(error)
    clearError()
  }, [error, message, clearError])

  const changeHandler = event => {
    setForm({ ...form, [event.target.name]: event.target.value })
  }
  const submitHandler = async () => {
    try {
      const id = new RandExp(/^[0-9a-fA-F]{24}$/).gen()

      const data = await request('/api/events/create-event', 'POST', { _id: id, ...form, owner: auth.userId })
      dispatch(addEvent({ id, ...form, owner: auth.userId }))
      message(data.message, true)
      onHide()
    } catch (e) {}
  }
  return (
    <Form>
      <Form.Group controlId='title'>
        <Form.Label>Email address</Form.Label>
        <Form.Control type='text' name='title' value={form.title} onChange={changeHandler} placeholder='Enter title' />
      </Form.Group>
      <Form.Group controlId='title'>
        <Form.Label>Star of event (in minutes)</Form.Label>
        <Form.Control type='text' name='start' value={form.start} onChange={changeHandler} placeholder='Enter start' />
      </Form.Group>
      <Form.Group controlId='title'>
        <Form.Label>Event duration (in minutes)</Form.Label>
        <Form.Control type='text' name='duration' value={form.duration} onChange={changeHandler} placeholder='Enter duration' />
      </Form.Group>
      <Button variant='primary' type='submit' onClick={submitHandler} disabled={loading}>
          Submit
      </Button>
    </Form>
  )
}
