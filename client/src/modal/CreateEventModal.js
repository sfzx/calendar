import React from 'react'
import { Modal } from 'react-bootstrap'
import { CreateEvent } from '../components/CreateEvent'

export const CreateEventModal = (props) => {
  return (
    <Modal
      {...props}
      size='lg'
      aria-labelledby='contained-modal-title-vcenter'
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id='contained-modal-title-vcenter'>
          Create Event
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <CreateEvent
          onHide={props.onHide}
        />
      </Modal.Body>
    </Modal>
  )
}
