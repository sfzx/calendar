import { combineReducers, createStore } from 'redux'

import eventReducer from './events/reducers'

const rootReducer = combineReducers({
  event: eventReducer
})

export const store = createStore(rootReducer)
