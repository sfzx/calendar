import { createSlice } from '@reduxjs/toolkit'

const initialState = [{
  id: '', title: '', start: 0, duration: 0, owner: ''
}]

const eventSlice = createSlice({
  name: 'event',
  initialState,
  reducers: {
    addEvent (state, action) {
      state.push(action.payload)
    },
    deleteEvent (state, action) {
      state.filter(event => event.id !== action.payload)
    }

  }
})

export const { addEvent, deleteEvent } = eventSlice.actions

export const selectAllEvents = (state) => state.event

export default eventSlice.reducer

export const ActionTypes = {
  ADD_EVENT: 'ADD_EVENT',
  DELETE_EVENT: 'DELETE_EVENT'
}

// export default eventReducer
// const eventReducer = (state = initialState, action) => {
//   switch (action.type) {
//     case 'ADD_EVENT':
//       return {
//         ...state,
//         event: [...state.event, action.payload]
//       }
//     case 'DELETE_EVENT':
//       return {
//         event: [
//           ...state.event.filter(event => event.id !== action.payload)
//         ]
//       }
//     default:
//       return state
//   }
// }
