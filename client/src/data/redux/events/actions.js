
import { ActionTypes } from './reducers'

export const addEvent = event => ({
  type: ActionTypes.ADD_EVENT,
  payload: event
})

export const deleteEvent = id => ({
  type: ActionTypes.DELETE_EVENT,
  payload: id
})
