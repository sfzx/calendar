import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { ToastProvider } from 'react-toast-notifications'
import { Provider } from 'react-redux'
import { useRoutes } from './routes'
import { useAuth } from './hooks/auth.hook'
import { AuthContext } from './context/AuthContext'
import { MyNavbar } from './components/Navbar'
import { Loader } from './components/Loader'
import { store } from './data/redux/store'

function App () {
  const { token, login, logout, userId, ready } = useAuth()
  const isAuthenticated = !!token
  const routes = useRoutes(isAuthenticated)
  if (!ready) {
    return <Loader />
  }

  return (
    <AuthContext.Provider value={{
      token, login, logout, userId, isAuthenticated
    }}
    >
      <Provider store={store}>
        <ToastProvider>
          <Router>
            <MyNavbar />
            <div className='container'>
              {routes}
            </div>
          </Router>
        </ToastProvider>
      </Provider>
    </AuthContext.Provider>
  )
}

export default App
