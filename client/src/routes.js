import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { LoginPage } from './pages/LoginPage'
import { RegisterPage } from './pages/RegisterPage'
import { MainPage } from './pages/MainPage'
import { NotFoundPage } from './pages/NotFoundPage'

export const useRoutes = isAuthenticated => {
  let notAuth
  if (!isAuthenticated) {
    notAuth =
      <>
        <Route path='/login' exact>
          <LoginPage />
        </Route>
        <Route path='/register' exact>
          <RegisterPage />
        </Route>
      </>
  } else {
    notAuth =
      <>
        <Redirect to='/' />
      </>
  }
  return (
    <Switch>
      <Route path='/' exact>
        <MainPage />
      </Route>
      {notAuth}
      <Route path='/404' exact>
        <NotFoundPage />
      </Route>
      <Redirect to='/404' />
    </Switch>
  )
}
