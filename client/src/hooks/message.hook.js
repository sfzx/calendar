import { useCallback } from 'react'
import { useToasts } from 'react-toast-notifications'

export const useMessage = () => {
  const { addToast } = useToasts()
  return useCallback((text, flag = false) => {
    if (text) {
      addToast(text, {
        appearance: flag ? 'success' : 'error',
        autoDismiss: true
      })
    }
  }, [addToast])
}
