export const downloadJSON = (data) => {
  const resJSON = []
  data.map(elem => resJSON.push({ start: elem.start, duration: elem.duration, title: elem.title }))
  const res = `data:text/json;charset=utf-8,${encodeURIComponent(JSON.stringify(resJSON).replace(/},/g, '},\n'))}`
  const a = document.createElement('a')
  a.href = res
  a.download = 'events.json'
  a.click()
}
