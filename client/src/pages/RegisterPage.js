import React, { useState, useEffect } from 'react'
import { Button, Form, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { useMessage } from '../hooks/message.hook'
import { useHttp } from '../hooks/http.hook'

export const RegisterPage = () => {
  const message = useMessage()
  const { loading, request, error, clearError } = useHttp()

  const [form, setForm] = useState({
    userName: '', email: '', password: ''
  })

  useEffect(() => {
    message(error)
    clearError()
  }, [error, message, clearError])

  const changeHandler = event => {
    setForm({ ...form, [event.target.name]: event.target.value })
  }

  const registerHandler = async () => {
    try {
      const data = await request('/api/auth/register', 'POST', { ...form })
      message(data.message, true)
      setForm({ userName: '', email: '', password: '' })
    } catch (e) {}
  }
  return (
    <Form className='auth-size'>
      <Form.Group controlId='userName'>
        <Form.Label>User name</Form.Label>
        <Form.Control type='userName' name='userName' value={form.userName} onChange={changeHandler} placeholder='Enter user name' />
      </Form.Group>

      <Form.Group controlId='email'>
        <Form.Label>Email address</Form.Label>
        <Form.Control type='email' name='email' value={form.email} onChange={changeHandler} placeholder='Enter email' />
        <Form.Text className='text-muted'>
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group controlId='password'>
        <Form.Label>Password</Form.Label>
        <Form.Control type='password' name='password' value={form.password} onChange={changeHandler} placeholder='Password' />
      </Form.Group>

      <Form.Row>
        <Col>
          <Button variant='primary' type='submit' onClick={registerHandler} disabled={loading}>
          Submit
          </Button>
        </Col>

        <Link to='/login'>Back to login</Link>
      </Form.Row>
    </Form>
  )
}
