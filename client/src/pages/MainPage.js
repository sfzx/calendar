import React, { useEffect, useCallback, useContext } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Row, Col } from 'react-bootstrap'
import { Event } from '../components/Event'
import { useHttp } from '../hooks/http.hook'
import { Loader } from '../components/Loader'
import { AuthContext } from '../context/AuthContext'
import { Calendar } from '../components/Calendar'
import { addEvent, selectAllEvents } from '../data/redux/events/reducers'

export const MainPage = () => {
  const auth = useContext(AuthContext)
  const { request, loading } = useHttp()
  const dispatch = useDispatch()

  const getContent = useCallback(async () => {
    try {
      const fetched = await request(`/api/events/get?q=${auth.userId}`, 'GET', null)

      const eve = []
      fetched.map(elem => eve.push({ id: elem._id, title: elem.title, start: elem.start, duration: elem.duration, owner: elem.owner }))
      eve.map(elem => dispatch(addEvent(elem)))
    } catch (error) {}
  }, [request, auth, dispatch])

  useEffect(() => {
    getContent()
  }, [getContent])

  const events = useSelector(selectAllEvents)

  if (loading) {
    return <Loader />
  }

  const time = [8, 9, 10, 11, 12, 1, 2, 3, 4, 5]

  return (
    <>
      <Row>
        <Col>
          {time && time.map(elem => <Calendar time={elem} key={elem} />)}
        </Col>

        {!!auth.token && !loading && events && events.map(elem => <Event data={elem} key={elem.id} />)}

      </Row>

    </>

  )
}
