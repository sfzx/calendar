import React, { useState, useEffect, useContext } from 'react'
import { Button, Form, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { useMessage } from '../hooks/message.hook'
import { useHttp } from '../hooks/http.hook'
import { AuthContext } from '../context/AuthContext'

export const LoginPage = () => {
  const auth = useContext(AuthContext)
  const message = useMessage()
  const { loading, request, error, clearError } = useHttp()
  const [form, setForm] = useState({
    email: '', password: ''
  })

  useEffect(() => {
    message(error)
    clearError()
  }, [error, message, clearError])

  const changeHandler = event => {
    setForm({ ...form, [event.target.name]: event.target.value })
  }

  const loginHandler = async () => {
    try {
      const data = await request('/api/auth/login', 'POST', { ...form })
      message(data.message, true)
      auth.login(data.token, data.userId)
    } catch (e) {}
  }

  return (
    <Form className='auth-size'>
      <Form.Group controlId='email'>
        <Form.Label>Email address</Form.Label>
        <Form.Control type='email' name='email' value={form.email} onChange={changeHandler} placeholder='Enter email' />
        <Form.Text className='text-muted'>
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group controlId='password'>
        <Form.Label>Password</Form.Label>
        <Form.Control type='password' name='password' value={form.password} onChange={changeHandler} placeholder='Password' />
      </Form.Group>

      <Form.Row>
        <Col>
          <Button variant='primary' type='submit' onClick={loginHandler} disabled={loading}>
          Submit
          </Button>
        </Col>

        <Link to='/register'>Register Now !</Link>
      </Form.Row>
    </Form>
  )
}
