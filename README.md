# Calendar

## Prerequisites
node version 12 or higher installed

## Installation
### For Linux system:
```
In server folder:
npm run init
```
### Else 
```
In server folder:
npm i
Then in client folder:
npm i
```

## Start
```
To start enter server direction and type:
npm start
```

## Running tests
```
Only for server side
npm test
```

## Technologies used
### Back-End
```
Express, mongoose, joi, morgan, mocha, chai, supertest, jsonwebtoken
```
### Front-End
```
React, bootstrap, redux
```