/* eslint-env mocha */
const request = require('supertest')
const { expect } = require('chai')
const RandExp = require('randexp')
const app = require('../server')
const authServices = require('../app/auth/auth.services')
const eventServices = require('../app/events/events.services')

const id = new RandExp(/^[0-9a-fA-F]{24}$/).gen()

describe('api/events', () => {
  let user, event
  before(async () => {
    user = await authServices.createUser({ userName: 'tester', email: 'test@test.com', password: 'password' })
    event = await eventServices.createEvent({ _id: id, title: 'test title', start: 20, duration: 20, owner: user._id })
  })

  after(async () => {
    await authServices.deleteUser({ _id: user._id })
  })

  describe('Create event', () => {
    it('On POST /create-event should create event with correct input', async () => {
      const res = await request(app)
        .post('/api/events/create-event')
        .send({
          _id: new RandExp(/^[0-9a-fA-F]{24}$/).gen(),
          title: 'test',
          start: 20,
          duration: 20,
          owner: user._id
        })
      expect(res.statusCode).to.be.equal(201)
      expect(res.body).to.be.an('object')
      expect(res.body.message).to.be.equal('Event created')
    })

    it('On POST /create-event should return error if incorrect field input', async () => {
      const res = await request(app)
        .post('/api/events/create-event')
        .send({
          nan: 'asd',
          title: 'test',
          start: 20,
          duration: 20,
          owner: user._id
        })
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.message).to.be.equal('"nan" is not allowed')
    })

    it('On POST /create-event should return error if incorrect field naming', async () => {
      const res = await request(app)
        .post('/api/events/create-event')
        .send({
          ttle: 'test',
          start: 20,
          duration: 20,
          owner: user._id
        })
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.message).to.be.equal('"title" is required')
    })

    it('On POST /create-event should return error if incorrect input', async () => {
      const res = await request(app)
        .post('/api/events/create-event')
        .send({
          title: 20,
          start: 20,
          duration: 20,
          owner: user._id
        })
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.message).to.be.equal('"title" must be a string')
    })
  })
  describe('Get events', () => {
    it('On GET / should return all user events', async () => {
      const res = await request(app)
        .get(`/api/events/get?q=${user._id}`)
      expect(res.statusCode).to.be.equal(201)
      expect(res.body).to.be.an('array')
    })

    it('On GET / should return error if incorrect user id', async () => {
      const res = await request(app)
        .get('/api/events/get?q=asfsdg')
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.message).to.be.equal('"q" with value "asfsdg" fails to match the valid mongo id pattern')
    })
  })

  describe('Delete event', () => {
    it('On DELETE /delete-event should return error if incorrect field input', async () => {
      const res = await request(app)
        .delete('/api/events/delete-event')
        .send({
          nice: 'Yes',
          eventId: event._id,
          owner: user._id
        })
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.message).to.be.equal('"nice" is not allowed')
    })

    it('On DELETE /delete-event should return error if incorrect field naming', async () => {
      const res = await request(app)
        .delete('/api/events/delete-event')
        .send({
          Id: event._id,
          owner: user._id
        })
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.message).to.be.equal('"eventId" is required')
    })

    it('On DELETE /delete-event should return error if incorrect input', async () => {
      const res = await request(app)
        .delete('/api/events/delete-event')
        .send({
          eventId: 'event._id',
          owner: user._id
        })
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.message).to.be.equal('"eventId" with value "event._id" fails to match the valid mongo id pattern')
    })

    it('On DELETE /delete-event should delete user event', async () => {
      const res = await request(app)
        .delete('/api/events/delete-event')
        .send({
          eventId: event._id,
          owner: user._id
        })
      expect(res.statusCode).to.be.equal(200)
      expect(res.body).to.be.an('object')
      expect(res.body.message).to.be.equal('Event deleted')
    })
  })
})
