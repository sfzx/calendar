/* eslint-env mocha */
const request = require('supertest')
const { expect } = require('chai')
const app = require('../server')
const authServices = require('../app/auth/auth.services')

describe('/api/auth', () => {
  describe('Registration', () => {
    it('On POST /register should create new user with correct input', async () => {
      const res = await request(app)
        .post('/api/auth/register')
        .send({
          userName: 'test',
          email: 'test@test.com',
          password: 'password'
        })
      expect(res.statusCode).to.be.equal(201)
      expect(res.body).to.be.an('object')
      expect(res.body.message).to.be.equal('User created')
    })

    it('On POST /register should return error if incorrect field input', async () => {
      const res = await request(app)
        .post('/api/auth/register')
        .send({
          name: 'test',
          userName: 'test',
          email: 'test1@test.com',
          password: 'password'
        })
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.status).to.be.equal('error')
      expect(res.body.message).to.be.equal('"name" is not allowed')
    })

    it('On POST /register should return error if incorrect field naming', async () => {
      const res = await request(app)
        .post('/api/auth/register')
        .send({
          userNae: 'test',
          email: 'test1@test.com',
          password: 'password'
        })
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.status).to.be.equal('error')
      expect(res.body.message).to.be.equal('"userName" is required')
    })

    it('On POST /register should return error if incorrect email format', async () => {
      const res = await request(app)
        .post('/api/auth/register')
        .send({
          userName: 'test',
          email: 'test1@com',
          password: 'password'
        })
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.status).to.be.equal('error')
      expect(res.body.message).to.be.equal('"email" must be a valid email')
    })

    it('On POST /register should return error if incorrect password format', async () => {
      const res = await request(app)
        .post('/api/auth/register')
        .send({
          userName: 'test',
          email: 'test1@gmail.com',
          password: 'pass'
        })
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.status).to.be.equal('error')
      expect(res.body.message).to.be.equal('Password must be at least 6 character long')
    })
  })

  describe('Login', () => {
    after(() => {
      authServices.deleteUser({ email: 'test@test.com' })
    })
    it('On POST /login should login user and create JWT', async () => {
      const res = await request(app)
        .post('/api/auth/login')
        .send({
          email: 'test@test.com',
          password: 'password'
        })
      expect(res.statusCode).to.be.equal(200)
      expect(res.body).to.be.an('object')
      expect(res.body).to.haveOwnProperty('token')
      expect(res.body).to.haveOwnProperty('userId')
      expect(res.body.message).to.be.equal('Hello test !')
    })

    it('On POST /login should return error if incorrect field input', async () => {
      const res = await request(app)
        .post('/api/auth/login')
        .send({
          email: 'test@test.com',
          password: 'password',
          name: 'test'
        })
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.status).to.be.equal('error')
      expect(res.body.message).to.be.equal('"name" is not allowed')
    })

    it('On POST /login should return error if incorrect field naming', async () => {
      const res = await request(app)
        .post('/api/auth/login')
        .send({
          emoil: 'test@test.com',
          password: 'password'
        })
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.status).to.be.equal('error')
      expect(res.body.message).to.be.equal('"email" is required')
    })

    it('On POST /login should return error if incorrect email format', async () => {
      const res = await request(app)
        .post('/api/auth/login')
        .send({
          email: 'tester@com',
          password: 'password'
        })
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.status).to.be.equal('error')
      expect(res.body.message).to.be.equal('"email" must be a valid email')
    })

    it('On POST /login should return error if no user with such email', async () => {
      const res = await request(app)
        .post('/api/auth/login')
        .send({
          email: 'tester@test.com',
          password: 'password'
        })
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.status).to.be.equal('error')
      expect(res.body.message).to.be.equal('Wrong email')
    })

    it('On POST /login should return error if incorrect password format', async () => {
      const res = await request(app)
        .post('/api/auth/login')
        .send({
          email: 'test@test.com',
          password: 'pasr'
        })
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.status).to.be.equal('error')
      expect(res.body.message).to.be.equal('Password must be at least 6 character long')
    })

    it('On POST /login should return error if wrong password', async () => {
      const res = await request(app)
        .post('/api/auth/login')
        .send({
          email: 'test@test.com',
          password: 'passworder'
        })
      expect(res.statusCode).to.be.equal(400)
      expect(res.body).to.be.an('object')
      expect(res.body.status).to.be.equal('error')
      expect(res.body.message).to.be.equal('Wrong password')
    })
  })
})
