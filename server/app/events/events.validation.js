const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const validateBody = require('../helpers/validateBody')

const createEvent = Joi.object({
  _id: Joi.objectId(),
  title: Joi.string().required(),
  start: Joi.number().required(),
  duration: Joi.number().required(),
  owner: Joi.objectId().required()
})

const deleteEvent = Joi.object({
  eventId: Joi.objectId().required(),
  owner: Joi.objectId().required()
})

const getEvent = Joi.object({
  q: Joi.objectId().required()
})

const eventValidation = {
  create: async (req, res, next) => {
    await validateBody(req.body, createEvent, next)
    await next()
  },
  delete: async (req, res, next) => {
    await validateBody(req.body, deleteEvent, next)
    await next()
  },
  get: async (req, res, next) => {
    await validateBody(req.query, getEvent, next)
    await next()
  }
}

module.exports = eventValidation
