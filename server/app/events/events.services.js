const Event = require('./events.model')
const User = require('../auth/user.model')

const eventServices = {
  createEvent: async (data) => {
    const event = await Event.create(data)
    await User.findByIdAndUpdate(data.owner, { $push: { userEvents: event._id } })
    return event
  },
  getUserEvents: (userId) => {
    return Event.find({ owner: userId })
  },
  deleteEvent: (data) => {
    return Event.deleteOne({ _id: data.eventId, owner: data.owner })
  }
}

module.exports = eventServices
