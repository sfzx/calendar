const { Router } = require('express')
const router = Router()
const eventServices = require('./events.services')
const eventValidation = require('./events.validation')

router.post('/create-event', eventValidation.create, async (req, res) => {
  await eventServices.createEvent(req.body)
  res.status(201).json({ message: 'Event created' })
})

router.get('/get?', eventValidation.get, async (req, res) => {
  const events = await eventServices.getUserEvents(req.query.q)
  res.status(201).json(events)
})

router.delete('/delete-event', eventValidation.delete, async (req, res) => {
  await eventServices.deleteEvent(req.body)
  res.status(200).json({ message: 'Event deleted' })
})

module.exports = router
