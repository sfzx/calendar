const { Schema, model } = require('mongoose')

const eventSchema = new Schema({
  _id: {
    type: Schema.Types.ObjectId
  },
  title: {
    type: String,
    required: true
  },
  start: {
    type: Number,
    required: true
  },
  duration: {
    type: Number,
    required: true
  },
  owner: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  createAt: {
    type: Date,
    default: Date.now
  }
})

module.exports = model('Event', eventSchema)
