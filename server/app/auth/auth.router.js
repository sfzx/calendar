const { Router } = require('express')
const authServices = require('./auth.services')
const authValidation = require('./auth.validation')
const router = Router()

// /api/auth/register
router.post('/register', authValidation.register, async (req, res) => {
  await authServices.createUser(req.body)
  res.status(201).json({ message: 'User created' })
})

// /api/auth/login
router.post('/login', authValidation.login, async (req, res) => {
  const data = await authServices.loginUser(req.body.email)
  res.status(200).json({ token: data.token, userId: data.userId, message: `Hello ${data.user} !` })
})

module.exports = router
