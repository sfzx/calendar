const { Schema, model } = require('mongoose')

const userSchema = new Schema({
  userName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  userEvents: [{
    type: Schema.Types.ObjectId,
    ref: 'Event'
  }],
  createAt: {
    type: Date,
    default: Date.now
  }
})

module.exports = model('User', userSchema)
