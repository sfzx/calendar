const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const User = require('./user.model')

const authServices = {
  createUser: async (data) => {
    data.password = await bcrypt.hash(data.password, 10)
    return User.create(data)
  },
  findUser: async (param) => {
    return User.findOne(param)
  },
  loginUser: async (email) => {
    const user = await User.findOne({ email })
    const token = jwt.sign(
      { userId: user.id },
      process.env.JWT_SECRET,
      { expiresIn: '1h' }
    )
    return { token, userId: user.id, user: user.userName }
  },
  comparePassword: (inputPassword, userPassword) => {
    return bcrypt.compare(inputPassword, userPassword)
  },
  deleteUser: async (param) => {
    return User.deleteOne(param)
  }
}

module.exports = authServices
