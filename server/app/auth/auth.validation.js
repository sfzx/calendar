const Joi = require('joi')
const validateBody = require('../helpers/validateBody')
const authServices = require('./auth.services')
const { ErrorHandler } = require('../helpers/error')

const createUser = Joi.object({
  userName: Joi.string().required(),
  email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'ua'] } }).required(),
  password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')).message('Password must be at least 6 character long').required()
})

const loginUser = Joi.object({
  email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'ua'] } }).required(),
  password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')).message('Password must be at least 6 character long').required()
})

async function checkEmail (email, next) {
  try {
    if (await authServices.findUser({ email })) {
      throw new ErrorHandler(400, 'Email already in use')
    }
  } catch (error) {
    next(error)
  }
}

async function validateUser (data, next) {
  try {
    const user = await authServices.findUser({ email: data.email })
    if (user) {
      if (!await authServices.comparePassword(data.password, user.password)) {
        throw new ErrorHandler(400, 'Wrong password')
      }
    } else {
      throw new ErrorHandler(400, 'Wrong email')
    }
  } catch (error) {
    next(error)
  }
}

const authValidation = {
  register: async (req, res, next) => {
    await validateBody(req.body, createUser, next)
    await checkEmail(req.body.email, next)
    await next()
  },
  login: async (req, res, next) => {
    await validateBody(req.body, loginUser, next)
    await validateUser(req.body, next)
    await next()
  }
}

module.exports = authValidation
