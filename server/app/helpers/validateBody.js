const { ErrorHandler } = require('./error')

module.exports = async (body, schema, next) => {
  try {
    await schema.validateAsync(body)
  } catch (error) {
    next(new ErrorHandler(400, error.details[0].message))
  }
}
