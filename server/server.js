require('dotenv').config()
const express = require('express')
const app = express()
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const auth = require('./app/auth/auth.router')
const events = require('./app/events/events.router')
const { handleError } = require('./app/helpers/error')

const PORT = process.env.PORT || 3100

mongoose.connect(process.env.MONGO_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
})
mongoose.set('debug', process.env.NODE_ENV !== 'production')
const db = mongoose.connection
db.on('error', error => console.error(error))
db.once('open', () => console.log('Connected to Mongoose'))

app.use(morgan('dev'))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: 'false' }))

app.use('/api/auth', auth)
app.use('/api/events', events)

app.use((err, req, res, next) => {
  handleError(err, res)
})

app.listen(PORT, () => {
  console.log(`server run on port ${PORT}`)
})

module.exports = app
